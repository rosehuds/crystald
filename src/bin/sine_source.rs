extern crate event;
extern crate syscall;

use std::os::unix::io::RawFd;

use syscall::*;

fn main() {
    let connect = std::env::args().nth(1).expect("argument 1 should be a sink name to connect to");
    let fd = open(format!("audio:sine_source?buf_sz=256&connect={}", connect), O_CREAT | O_WRONLY).unwrap();
    let addr = unsafe { fmap(fd, 0, 1024) }.unwrap();
    let buffer: &'static mut [i32] = unsafe { std::slice::from_raw_parts_mut(addr as *mut i32, 256) };
    println!("buffer view: {:?}", &buffer[0..16]);
    let mut event_loop = event::EventQueue::<(), std::io::Error>::new().unwrap();
    let mut phase = 0.;
    event_loop.add(fd as RawFd, move |_| {
        println!("starting phase: {}", phase);
        for val in buffer.iter_mut() {
            *val = ((phase * std::f32::consts::PI * 2.0).sin() * (i32::max_value() / 16) as f32) as i32;
            phase += 0.05;
        }
        Ok(None)
    }).unwrap();
    event_loop.run().unwrap();
}
