use std::collections::{BTreeSet, HashMap};
use std::rc::Rc;
use std::cell::RefCell;
use std::fs::File;
use std::io::Write;
use std::str;

use syscall;
use syscall::scheme::SchemeMut;
use syscall::data::Packet;
use syscall::flag::*;
use syscall::error::*;
use rand::random;

use buffer::AudioBuffer;

const DEFAULT_BUFFER_SIZE: usize = 512; // samples

struct Endpoint {
    name: String,
    buffer: AudioBuffer<i32>,
    is_mapped: bool,
    connections: BTreeSet<usize>,
    endpoint_type: EndpointType,
    clock_source: Option<usize>,
}

#[derive(PartialEq)]
enum EndpointType {
    Source,
    Sink,
}

pub struct AudioScheme {
    scheme_file: Rc<RefCell<File>>,
    endpoint_name_to_id: HashMap<String, usize>,
    endpoints: HashMap<usize, Endpoint>,
    used_file_ids: BTreeSet<usize>,
}

impl AudioScheme {
    pub fn new(scheme_file: Rc<RefCell<File>>) -> Self {
        AudioScheme {
            scheme_file,
            endpoint_name_to_id: HashMap::new(),
            endpoints: HashMap::new(),
            used_file_ids: BTreeSet::new(),
        }
    }
}

impl SchemeMut for AudioScheme {
    fn open(&mut self, path: &[u8], flags: usize, _uid: u32, _gid: u32) -> Result<usize> {
        let path = str::from_utf8(path).or(Err(Error::new(EINVAL)))?;

        let file_id = gen_file_id(&mut self.used_file_ids);
        if flags & O_CREAT == O_CREAT {
            let endpoint = match flags & O_RDWR {
                O_RDONLY => Endpoint {
                    name: path.to_owned(),
                    buffer: AudioBuffer::new(DEFAULT_BUFFER_SIZE),
                    is_mapped: false,
                    connections: BTreeSet::new(),
                    endpoint_type: EndpointType::Sink,
                    clock_source: None,
                },
                O_WRONLY => Endpoint {
                    name: path.to_owned(),
                    buffer: AudioBuffer::new(DEFAULT_BUFFER_SIZE),
                    is_mapped: false,
                    connections: BTreeSet::new(),
                    endpoint_type: EndpointType::Source,
                    clock_source: None,
                },
                _ => return Err(Error::new(EINVAL)),
            };
/*             if let Some(conn_names) = args.get("connect") {
                let conn_names = conn_names.split(',');
                for conn_name in conn_names {
                    let conn_id = self.endpoint_name_to_id.get(conn_name).ok_or(Error::new(EINVAL))?;
                    endpoint.connections.insert(*conn_id);
                    self.endpoints.get_mut(conn_id).unwrap().connections.insert(file_id);
                }
            } */
            self.endpoints.insert(file_id, endpoint);
            self.endpoint_name_to_id.insert(path.to_owned(), file_id);
        } else {
            return Err(Error::new(EINVAL));
        }
        Ok(file_id)
    }

    fn read(&mut self, id: usize, mut buf: &mut [u8]) -> Result<usize> {
        use std::io::Write;
        let initial_buf_len = buf.len();
        let endpoint = self.endpoints.get(&id).ok_or(Error::new(EBADF))?;
        buf.write(format!("buffer_size {}\n", endpoint.buffer.size).as_bytes()).unwrap(); // can't fail
        for connection in &endpoint.connections {
            let other_end = self.endpoints.get(&connection).unwrap();
            buf.write(format!("connection {}\n", other_end.name).as_bytes()).unwrap();
        }

        Ok(initial_buf_len - buf.len() as usize)
    }

    fn write(&mut self, id: usize, buf: &[u8]) -> Result<usize> {
        let buf = str::from_utf8(buf).or(Err(Error::new(EINVAL)))?;
        let mut endpoint = self.endpoints.remove(&id).ok_or(Error::new(EBADF))?;
        for command in buf.split('\n') {
            let (verb, args) = {
                let mut iter = command.splitn(2, ' ');
                (iter.next().unwrap(), iter.next())
            };
            match verb {
                "connect" => {
                    let conn_id = self.endpoint_name_to_id.get(args.ok_or(Error::new(EINVAL))?)
                        .ok_or(Error::new(ENOENT))?;
                    endpoint.connections.insert(*conn_id);
                    let other_end = self.endpoints.get_mut(conn_id).unwrap();
                    if other_end.endpoint_type == endpoint.endpoint_type {
                        return Err(Error::new(EINVAL));
                    }
                },
                "match" => {
                    if endpoint.is_mapped { return Err(Error::new(EBUSY)) }
                    let other_end_id = self.endpoint_name_to_id.get(args.ok_or(Error::new(EINVAL))?)
                        .ok_or(Error::new(ENOENT))?;
                    let other_end = self.endpoints.get(&other_end_id).unwrap();
                    
                },
                _ => return Err(Error::new(EINVAL)),
            }
        }
        self.endpoints.insert(id, endpoint);
        Ok(buf.len())
    }

    fn fevent(&mut self, id: usize, _flags: usize) -> Result<usize> {
        if self.used_file_ids.contains(&id) {
            Ok(id)
        } else {
            Err(Error::new(EBADF))
        }
    }

    fn fmap(&mut self, id: usize, offset: usize, size: usize) -> Result<usize> {
        if !self.used_file_ids.contains(&id) {
            return Err(Error::new(EBADF));
        }
        if offset != 0 {
            return Err(Error::new(EINVAL));
        }

        if let Some(endpoint) = self.endpoints.get_mut(&id) {
            if size != endpoint.buffer.len() * 4 {
                return Err(Error::new(EINVAL));
            }
            endpoint.is_mapped = true;
            return Ok(endpoint.buffer.as_ptr() as usize);
        } else {
            return Err(Error::new(EBADF));
        }
    }

    fn fsync(&mut self, id: usize) -> Result<usize> {
        let mut endpoint = self.endpoints.remove(&id).ok_or(Error::new(EBADF))?;
        match endpoint.endpoint_type {
            EndpointType::Source => for file_id in endpoint.connections.iter() {
                if self.endpoints.get(file_id).unwrap().clock_source == Some(id) {
                    self.scheme_file
                        .borrow_mut()
                        .write(&Packet {
                            id: 0,
                            pid: 0,
                            uid: 0,
                            gid: 0,
                            a: syscall::number::SYS_FEVENT,
                            b: *file_id,
                            c: syscall::EVENT_READ,
                            d: endpoint.buffer.len(),
                        })
                        .expect("failed to write to scheme file");
                }
            },
            EndpointType::Sink => {
                for val in endpoint.buffer.iter_mut() {
                    *val = 0;
                }
                // mixing time!
                for file_id in endpoint.connections.iter() {
                    let connected = self.endpoints.get_mut(&file_id).unwrap();
                    for (idx, val) in endpoint.buffer.iter_mut().enumerate() {
                        *val += connected.buffer[idx];
                    }

                    if connected.clock_source == None {
                        connected.clock_source = Some(id);
                    }
                    if connected.clock_source == Some(id) {
                        self.scheme_file
                            .borrow_mut()
                            .write(&Packet {
                                id: 0,
                                pid: 0,
                                uid: 0,
                                gid: 0,
                                a: syscall::number::SYS_FEVENT,
                                b: *file_id,
                                c: syscall::EVENT_WRITE,
                                d: endpoint.buffer.len(),
                            })
                            .expect("failed to write to scheme file");
                    }
                }
            }
        }
        self.endpoints.insert(id, endpoint);
        Ok(0)
    }

    fn close(&mut self, id: usize) -> Result<usize> {
        if !self.used_file_ids.remove(&id) {
            return Err(Error::new(EBADF));
        }
        if let Some(endpoint) = self.endpoints.remove(&id) {
            self.endpoint_name_to_id.remove(&endpoint.name);
            for conn_id in endpoint.connections {
                self.scheme_file
                    .borrow_mut()
                    .write(&Packet {
                        id: 0,
                        pid: 0,
                        uid: 0,
                        gid: 0,
                        a: syscall::number::SYS_FEVENT,
                        b: conn_id,
                        c: syscall::EVENT_WRITE,
                        d: 0, // zero-size buffer, endpoint is closed
                    })
                    .expect("failed to write to scheme file");
                self.endpoints.get_mut(&conn_id).unwrap().connections.remove(&id);
            }
            return Ok(0);
        }

        panic!("file id marked as used but wasn't");
    }
}

fn gen_file_id(used_ids: &mut BTreeSet<usize>) -> usize {
    loop {
        let id = random();
        if !used_ids.contains(&id) {
            used_ids.insert(id);
            return id;
        }
    }
}
